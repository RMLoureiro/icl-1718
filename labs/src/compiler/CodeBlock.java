package compiler;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

public class CodeBlock {
	
	static class StackFrame {
		void dump() {
			/*
			 * frame_id.j
			 * 
.class frame_id
.super java/lang/Object
.field public SL Lancestor_frame_id; 
.field public loc_00 type;
.field public loc_01 type;
..
.field public loc_n type;
.end method 
			 */
		}
	}
	
	ArrayList<String> code;
	// ArrayList<StackFrame> frames;
	public static final LabelFactory labelFactory = new LabelFactory();

	public CodeBlock() {
		code = new ArrayList<String>(100);
	}

	public void emit_push(int n) {
		code.add("sipush "+n);
	}

	public void emit_add() {
		code.add("iadd");
	}

	public void emit_mul() {
		code.add("imul");
	}

	public void emit_div() {
		code.add("idiv");
	}

	public void emit_sub() {
		code.add("isub");
	}

	void dumpHeader(PrintStream out) {
		out.println(".class public Demo");
		out.println(".super java/lang/Object");
		out.println("");
		out.println(";");
		out.println("; standard initializer");
		out.println(".method public <init>()V");
		out.println("   aload_0");
		out.println("   invokenonvirtual java/lang/Object/<init>()V");
		out.println("   return");
		out.println(".end method");
		out.println("");
		out.println(".method public static main([Ljava/lang/String;)V");
		out.println("       ; set limits used by this method");
		out.println("       .limit locals 10");
		out.println("       .limit stack 256");
		out.println("");
		out.println("       ; setup local variables:");
		out.println("");
		out.println("       ;    1 - the PrintStream object held in java.lang.out");
		out.println("       getstatic java/lang/System/out Ljava/io/PrintStream;");
		out.println("");
		out.println("       ; place your bytecodes here");
		out.println("       ; START");
		out.println("");
	}

	void dumpFooter(PrintStream out) {
		out.println("       ; END");
		out.println("");
		out.println("");		
		out.println("       ; convert to String;");
		out.println("       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
		out.println("       ; call println ");
		out.println("       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
		out.println("");		
		out.println("       return");
		out.println("");		
		out.println(".end method");
	}

	void dumpCode(PrintStream out) {
		for( String s : code )
			out.println("       "+s);
	}
	
	public void dump(String filename) throws FileNotFoundException {
		PrintStream out = new PrintStream(new FileOutputStream(filename));
		dumpHeader(out);
		dumpCode(out);
		dumpFooter(out);
//		dumpFrames();
	}

//	private void dumpFrames() {
//		for( Stackframe frame: frames)
//			frame.dump();
//	}

	public void emit_bool(boolean val) {
		if(val)
			code.add("iconst_1");
		else 
			code.add("iconst_0");
	}

	public void emit_ifeq(String label) {
		code.add("ifeq "+label);
	}

	public void emit_jump(String label) {
		code.add("goto "+label);
	}

	public void emit_anchor(String label) {
		code.add(label + ":");
	}
}