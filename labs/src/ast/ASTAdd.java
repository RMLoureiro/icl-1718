package ast;

<<<<<<< HEAD
=======
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.IValue;
import compiler.CodeBlock;
import types.IType;
import types.IntType;
import types.TypingException;
>>>>>>> 30a44701898d49c23802b281880f56962730777a
import values.IValue;
import values.IntValue;

public class ASTAdd implements ASTNode {

	ASTNode left, right;

	public ASTAdd(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public String toString() {
		return left.toString() + " + " + right.toString();
	}

	@Override
<<<<<<< HEAD
	public IValue eval() {
		IValue l = left.eval();
		IValue r = right.eval();
=======
	public IValue eval(IEnvironment<IValue> env) throws DuplicateIdentifierException, UndeclaredIdentifierException {
		IValue l = left.eval(env);
		IValue r = right.eval(env);
>>>>>>> 30a44701898d49c23802b281880f56962730777a
		if( l instanceof IntValue && r instanceof IntValue)
			return new IntValue(((IntValue)l).getValue()+((IntValue)r).getValue());
		else 
			return null; // TODO WRONG AND INCOMPLETE
//		else 
//			throw new TypeMismatchException("Wrong types in add");
<<<<<<< HEAD
=======
	}

	@Override
	public IType typecheck(IEnvironment<IType> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		IType l = left.typecheck(env);
		IType r = right.typecheck(env);
		
		if( l == IntType.singleton && 
			r == IntType.singleton )
			return IntType.singleton;
//		else 
//			return new NoneType(); 
		else 
			throw new TypingException("Wrong types in add");
	}

	@Override
	public void compile(CodeBlock code) {
		left.compile(code);
		right.compile(code);
		code.emit_add();
>>>>>>> origin/solutions4
>>>>>>> 30a44701898d49c23802b281880f56962730777a
	}
}