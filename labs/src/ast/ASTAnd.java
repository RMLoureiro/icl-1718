package ast;

import compiler.CodeBlock;
import types.IType;
import types.IntType;
import types.TypingException;
import values.IValue;
import values.IntValue;

public class ASTAnd implements ASTNode {

	ASTNode left, right;

	public ASTAnd(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public String toString() {
		return left.toString() + " && " + right.toString();
	}

	@Override
	public IValue eval() {
		return null;  // << Your job
	}

	@Override
	public IType typecheck() throws TypingException {
		return null; // << Your job
	}

	@Override
	public void compile(CodeBlock code) {
		
		String labelFalse, labelExit;
		
		labelFalse = code.labelFactory.getLabel();
		labelExit = code.labelFactory.getLabel();
		
		left.compile(code);
				
		code.emit_ifeq(labelFalse);
		
		right.compile(code);		
						
		code.emit_ifeq(labelFalse);

		code.emit_bool(true);
		
		code.emit_jump(labelExit);
		
		code.emit_anchor(labelFalse);

		code.emit_bool(false);
		
		code.emit_anchor(labelExit);
	}
}
