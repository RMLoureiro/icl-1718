package tests;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

import main.Console;
import parser.ParseException;
import types.BoolType;
import types.IType;
import types.IntType;
import values.IValue;
import values.IntValue;

import values.IValue;
import values.IntValue;

public class InterpreterTests {

<<<<<<< HEAD
	private void testCase(String expression, IValue value) throws ParseException {
		assertTrue(Console.acceptCompare(expression,value));		
	}
	
	private void testNegativeCase(String expression, IValue value) throws ParseException {
	}
	
	 private void testCaseBool(String expression, boolean value) throws ParseException {
		assertTrue(Console.acceptCompareBool(expression, new BoolValue(value)));		
	 }
	
	 private void testNegativeCaseBool(String expression, boolean value) throws ParseException {
	 	assertFalse(Console.acceptCompareBool(expression, new BoolValue(value)));
	 }
	
	@Test
	public void test01() throws Exception {
		testCase("1\n",new IntValue(1));
		testCase("1+2\n",new IntValue(3));
		testCase("1-2-3\n",new IntValue(-4));
=======
	private void testCase(String expression, IType type) throws ParseException {
		assertTrue(Console.acceptCompareTypes(expression,type));		
	}
	
	private void testNegativeCase(String expression, IType type) throws ParseException {
		assertFalse(Console.acceptCompareTypes(expression,type));
	}
		
	@Test
	public void test01() throws Exception {
		testCase("1\n",IntType.singleton);
		testCase("99\n",IntType.singleton);
		testCase("10+23\n",IntType.singleton);
		testCase("1-2-3\n",IntType.singleton);
>>>>>>> 30a44701898d49c23802b281880f56962730777a
	}
	
	@Test
	public void testsLabClass02() throws Exception {
<<<<<<< HEAD
		testCase("4*2\n",new IntValue(8));
		testCase("4/2/2\n",new IntValue(1));
		testCase("-1\n",new IntValue(-1));
		// more tests for boolean values
=======
		 testCase("true\n",new BoolType());
		 testCase("false\n",new BoolType());
		 testCase("11 < 22\n", new BoolType());
		 testCase("11 > 22\n", new BoolType());
	}
	
	@Test
	public void testsLabClass05() throws Exception {
		testCase("decl x = 1 in x+1 end\n",2);
		testCase("decl x = 1 in decl y = 2 in x+y end end\n",3);
		testCase("decl x = decl y = 2 in 2*y end in x*3\n",4);
		testCase("decl x = 1 in x+2 end * decl y = 1 in 2*y end\n", 5);
		testNegativeCase("x+1", 0);
		testNegativeCase("decl x = 1 in x+1 end + x", 0);
		testCase("decl x = 1 in x+2 end * decl y = 1 in 2*y+x end\n", 5);		
>>>>>>> 30a44701898d49c23802b281880f56962730777a
	}
}
