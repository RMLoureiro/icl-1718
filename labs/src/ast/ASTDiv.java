package ast;

<<<<<<< HEAD
=======
import compiler.CodeBlock;
import types.IType;
import types.TypingException;
>>>>>>> 30a44701898d49c23802b281880f56962730777a
import values.IValue;
import values.IntValue;

public class ASTDiv implements ASTNode {

	ASTNode left, right;

	public ASTDiv(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public String toString() {
		return left.toString() + " / " + right.toString();
	}

	@Override
	public IValue eval() {
		IValue l = left.eval();
		IValue r = right.eval();
		if( l instanceof IntValue && r instanceof IntValue)
			return new IntValue(((IntValue)l).getValue() / ((IntValue)r).getValue());
		else 
			return null; // TODO WRONG AND INCOMPLETE
//		else 
//			throw new TypeMismatchException("Wrong types in add");
	}
<<<<<<< HEAD
=======

	@Override
	public IType typecheck() throws TypingException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void compile(CodeBlock code) {
		left.compile(code);
		right.compile(code);
		code.emit_div();
	}
>>>>>>> 30a44701898d49c23802b281880f56962730777a
}
