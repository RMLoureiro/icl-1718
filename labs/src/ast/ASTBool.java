package ast;

<<<<<<< HEAD
=======
import compiler.CodeBlock;
import types.IType;
import types.TypingException;
>>>>>>> 30a44701898d49c23802b281880f56962730777a
import values.BoolValue;
import values.IValue;

public class ASTBool implements ASTNode {

	boolean val;

	public ASTBool(boolean n) {
		val = n;
	}

	@Override
	public String toString() {
		return Boolean.toString(val);
	}

	@Override
	public IValue eval() {
		return new BoolValue(val);
	}

<<<<<<< HEAD
}

=======
	@Override
	public IType typecheck() throws TypingException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void compile(CodeBlock code) {
		code.emit_bool(val);
	}
}
>>>>>>> 30a44701898d49c23802b281880f56962730777a
